# customer-Managment

## Name
Customer Purchases Management (Flask-Kafka)

## Description
The web app save all users purchases in mongoDB database and display them by userid when making a query. 
The purchases message send throw producer to kafka container. 
The consumer get the purchases messages and make a query to the database.

## Badges
kafka:

Public Docker hub repository: noyz1/kafka:dns

zookeeper:

Public Docker hub repository: wurstmeister/zookeeper:latest

purchase:

Public Docker hub repository: noyz1/purchase:latest

## Installation
Install docker, docker-compose to running the project containers, install git to clone the project from repo.

- [ ] git clone https://gitlab.com/noy5277/customer-managment.git
- [ ] dokcer-compose -f docker-compose.yml up -d
- [ ] docker run -itd --name=purchase -p 5000:5000 -p 5001:5001 -p 5002:5002 --network customer-managment_default noyz1/purchase:latest 

## Usage
main page:

http://[ip address host]:5000

- There is two buttons on the up bar that render the pages to create, cart.

Create purchase request:

http://[ip address host]:5000/create

- Enter username, userid, price. click on submit to send the request.

Show purchases by userid:

http://[ip address host]:5000/cart

- Enter userid and click submit to get the purchases table.

## Contributing
If you want to change the user that connect to database ,Copy the .env-sample, and fill the database environment variables username and password.

For debugging kafka, you can connect the container with bash.
docker exec -it kafka bash
cd /opt/kafka_2.13-2.7.1/bin

debugging commands:
- kafka-topics.sh --list --zookeeper zookeeper:2181 | check the configured topics
- kafka-console-producer.sh --bootstrap-server localhost:9092 --topic messages | generate massages to producer
- kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic messages | check messages.
- kafka-topics.sh --delete --topic "topic name" --zookeeper zookeeper:2181

For checking the MongoDB database:

connect to: https://cloud.mongodb.com/v2/627133030facb91d9a564250#clusters

read permissions:

username: readpurchasesmongodb@gmail.com

password: 9KmxBYvInhAmdXDk
