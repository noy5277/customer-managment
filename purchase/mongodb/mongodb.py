import pymongo
import datetime
import os


class DB:
    def __init__(self, connection):
        self.connection = connection
        self.col = None

    def Connect(self):
        client = pymongo.MongoClient(self.connection)
        db = client["Store"]
        self.col = db["Purchases"]
        print ("Connect to database...")
        return self

    def Write(self, message):
        query = self.col.insert_one(message)
        return query.inserted_id



    #def Write(self, username, userid, price, timestamp):
    #    purchase = {"username": username, "userid": userid, "price": price, "timestamp": timestamp}
    #    query = self.col.insert_one(purchase)
    #    return query.inserted_id

    def FindById(self, userid):
        return self.col.find({"userid": userid})

    def FindByUserName(self, username):
        return self.col.find({"username": username})

    def GetAllPurchases(self):
        return self.col.find()



### test ####
#db_user = os.environ.get('DB_USER')
#db_password = os.environ.get('DB_PASS')

#db = DB("mongodb+srv://" + db_user + ":" + db_password + "@cluster0.rpp9t.mongodb.net"
#                                                         "/myFirstDatabase?retryWrites=true&w=majority")
#db.Create()
#print(db.Write("noy", "123", "12", datetime.datetime.now()))
#print(db.GetAllPurchases())
