from flask import Flask, render_template, request, url_for, flash, redirect, abort
import time
import json
import random
import requests
from datetime import datetime
from kafka import KafkaProducer

app = Flask(__name__)

def serializer(message):
    return json.dumps(message).encode('utf-8')

producer = KafkaProducer(
        bootstrap_servers=['kafka:9092'],
        value_serializer=serializer
)

def Write_to_producer(purchase):
    producer.send('messages',purchase)
    time_to_sleep = random.randint(1, 11)
    time.sleep(time_to_sleep)

@app.route('/')
def index():
    return render_template('index.html')



@app.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        username = request.form['username']
        userid = request.form['userid']
        price = request.form['price']
        timestamp=datetime.now()

        if not username:
            abort(404, "something wrong!!")
        elif not userid:
            abort(404, "something wrong!!")
        elif not price:
            abort(404, "something wrong!!")
        else:
            purchase={'username': username, 'userid': userid, 'price': price, 'timestamp': str(timestamp)}
            Write_to_producer(purchase)

    return render_template('create.html')

@app.route('/cart', methods=('GET', 'POST'))
def cart():
    if request.method == 'POST':
        userid = request.form['userid']

        if not userid:
            abort(404, "something wrong!!")
        else:
            r = requests.get(f"http://localhost:5001/read/{userid}")
            if r.status_code == 200 and len(r.json()) != 0:
                result=dict(r.json())
                return render_template("result.html",result=result)
            else:
                abort(404, "something wrong!!")
    return render_template('cart.html')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
