import json
import os
from managment import Write_From_Consumer
from mongodb.mongodb import DB
from flask import Flask

app = Flask(__name__)

db_user = os.environ.get("DB_USER")
db_password = os.environ.get("DB_PASS")  # os.environ['DB_PASS']

db = DB("mongodb+srv://" + db_user + ":" + db_password + "@cluster0.rpp9t.mongodb.net"
                                                             "/myFirstDatabase?retryWrites=true&w=majority")

if __name__ == '__main__':
    db.Connect()
    Write_From_Consumer(db)
    app.run(host="0.0.0.0", port=5002)
