from flask import Flask
import os
from threading import Thread
import json
from bson import json_util
from mongodb.mongodb import DB
from kafka import KafkaConsumer
from kafka import KafkaProducer
import datetime

app = Flask(__name__)

db_user = os.environ.get("DB_USER")
db_password = os.environ.get("DB_PASS")  # os.environ['DB_PASS']

db = DB("mongodb+srv://" + db_user + ":" + db_password + "@cluster0.rpp9t.mongodb.net"
                                                             "/myFirstDatabase?retryWrites=true&w=majority")

# write to db

def Write_From_Consumer(database):
    consumer = KafkaConsumer(
        'messages',
        bootstrap_servers='kafka:9092',
        auto_offset_reset='earliest'
    )
    for message in consumer:
        database.Write(json.loads(message.value))


@app.route("/read/<userid>", methods=['get'])
def router_get_userid(userid):
    result = dict()
    i=0
    for x in db.FindById(userid):
        result[i] = x
        i=i+1
    return json_util.dumps(result)

@app.route("/buylist", methods=['get'])
def router_get_all():
    result=dict()
    i=0
    for x in db.GetAllPurchases():
        result[i]=x
        i=i+1
    return json_util.dumps(result)

if __name__ == '__main__':

    db.Connect()
    db.Write({"username": "username", "userid": "userid", "price": "price", "timestamp": "timestamp"})

    app.run(host="0.0.0.0", port=5001)

